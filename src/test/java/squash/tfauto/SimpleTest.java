package squash.tfauto;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.opentestfactory.util.ParameterService;

public class SimpleTest {
 
    @Test 
    public void doNothingTest() {

    }
    @Test
    public void testCustomField() {
        String cufValue = ParameterService.INSTANCE.getString("TC_CUF_customfield", "defaultvalue");
        Assertions.assertEquals("value", cufValue, "The String value is incorrect, current is " + cufValue);
    }

    @Test
    public void testDataSet() {
        String dsName = ParameterService.INSTANCE.getString("DSNAME", "defaultname");
        String dsFieldValue = ParameterService.INSTANCE.getString("DS_field", "defaultvalue");

        Assertions.assertEquals("name", dsName, "The String value is incorrect, current is " + dsName);
        Assertions.assertEquals("value",dsFieldValue, "The String value is incorrect, current i s" + dsFieldValue);
    }
}
