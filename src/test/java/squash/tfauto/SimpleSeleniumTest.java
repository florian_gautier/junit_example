package squash.tfauto;

import java.util.concurrent.TimeUnit;

import org.junit.jupiter.api.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.opentestfactory.util.ParameterService;

import io.github.bonigarcia.wdm.WebDriverManager;


public class SimpleSeleniumTest {

    @Test
    public void chromeTest() {
        WebDriverManager.chromedriver().setup();
        WebDriver driver = new ChromeDriver();
        driver.get("https://autom-devops-fr.doc.squashtest.com/2022-02/index.html");
        driver.manage().timeouts().implicitlyWait(1, TimeUnit.SECONDS);
        driver.quit();
    }

    @Test
    public void multiBrowser() {

        String browserName = ParameterService.INSTANCE.getString("DS_browser", "chrome");

        WebDriver driver;

        switch (browserName ) {
            case "firefox":
                WebDriverManager.firefoxdriver().setup();
                driver = new FirefoxDriver();
                break;
            case "chrome":
            default:
                WebDriverManager.chromedriver().setup();
                driver = new ChromeDriver();
                break;
        }

        driver.get("https://autom-devops-fr.doc.squashtest.com/2022-02/index.html");
        driver.manage().timeouts().implicitlyWait(1, TimeUnit.SECONDS);
        driver.quit();
    }
    
}
